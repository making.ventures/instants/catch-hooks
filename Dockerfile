FROM node:alpine

RUN mkdir /srv/app
WORKDIR /srv/app

COPY package.json yarn.lock ./
RUN yarn && yarn cache clean
COPY . .
RUN yarn build

CMD ["yarn", "start"]
