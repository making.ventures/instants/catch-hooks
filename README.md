
This is service to catch web hooks

Hooks should be sended to '/hook' path.

/list will list all captured hooks, /find will find one. Both paths accepts filter passed through query or body (json or url-encoded).

Hooks stored in mongo. Env variable MONGO_URI should be passed.

# Start for dev

```
yarn dev
```

# Build, start

```
yarn build
yarn start
```

# Docker

```
docker build -t catch-hooks --no-cache .

docker rm -f catch-hooks || true && \
docker run \
    --name catch-hooks \
    -p 3000:3000 \
    -e MONGO_URI=$MONGO_URI \
    -it \
    catch-hooks
```
