import express from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';
import {MongoClient} from 'mongodb';
import log from 'loglevel';
import {jstr} from '@instants/core';

const mongoUri = process.env.MONGO_URI;
log.setDefaultLevel(log.levels.INFO);
log.info('mongoUri', mongoUri);

const getCollection = async () => {
  const client = new MongoClient(mongoUri, {useNewUrlParser: true});
  await client.connect();

  return client.db('catch-hooks').collection('catched');
};

const app = async () => {
  // Create collection connection
  const collection = await getCollection();

  // Create a new express app instance
  const app: express.Application = express();

  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  const upload = multer();
  app.use(upload.none());

  const hookHandler = async (req, res) => {
    const obj = {
      url: req.url,
      ...req.headers,
      ...req.query,
      ...req.params,
      ...req.body,
      dateCreated: new Date(),
      method: req.method,
    };

    await collection.save(obj);

    res.send(`data: ${jstr(obj)}`);
  };

  const listHandler = async (req, res) => {
    const composed = {
      ...req.query,
      ...req.params,
      ...req.body,
    };

    const {limit, ...search} = composed;

    let query = collection
      .find(search)
      .sort({dateCreated: -1});

    const limitNumber = parseInt(limit, 10);
    if (limitNumber && !isNaN(limitNumber)) {
      query = query.limit(limitNumber);
    }

    const result = await query.toArray();

    res.send(result);
  };

  const findHandler = async (req, res) => {
    const composed = {
      ...req.query,
      ...req.params,
      ...req.body,
    };

    const {...search} = composed;

    const obj = await collection.findOne(search, {sort: {dateCreated: -1}});

    res.send(obj);
  };

  app.all('/hook', hookHandler);
  app.all('/hook/*', hookHandler);
  app.all('/find', findHandler);
  app.all('/list', listHandler);
  app.all('/', listHandler);

  app.listen(3000, () => {
    log.info('App is listening on port 3000!');
  });
};

app();
